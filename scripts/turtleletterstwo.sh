rosservice call /reset
rosservice call /turtle1/set_pen 0 255 0 4 1
rosservice call /turtle1/teleport_absolute 4.5 5.5 0
rosservice call /turtle1/set_pen 0 255 0 4 0
rosservice call /spawn 8 5.5 0 ""
rosservice call /turtle2/set_pen 255 0 0 4 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-0.5, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-1, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0, 0.0]' '[0.0, 0, 1]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1, 1, 0.0]' '[0.0, 0, 3.1]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0, 0.0]' '[0.0, 0, -1]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0.85, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'
