from __future__ import print_function
from six.moves import input

#import all necessary packages
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p,q):
        return sqrt(sum((p_i - q_i)**2.0 for p_i, q_i in zip(p,q)))

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#initialize commander package
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous = True)

#setup ur5 robot group and interface
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
group = moveit_commander.MoveGroupCommander(group_name)

#plan to move robot arm up to location where it will have more room to move
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.4

#plan and move to target pose
group.set_pose_target(pose_goal)
plan = group.go(wait=True)
group.stop()
group.clear_pose_targets()

#plan pose for robot starting location of letter (0.4,0.1,0.1)
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.1

#plan and move to target pose
group.set_pose_target(pose_goal)
plan = group.go(wait=True)
group.stop()
group.clear_pose_targets()

#begin planning for letter A
waypoints = []
scale = 1 # 1 cm

#Left line up /
wpose = group.get_current_pose().pose #plan from starting pose
wpose.position.z += scale * 0.4  # Move up (z) by 0.4
wpose.position.y += scale * 0.2  # and sideways (y) by 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Right line down \
wpose.position.y += scale * 0.2  # Move sideways (y) by 0.2
wpose.position.z -= scale * 0.4  # Move down (z) by 0.4
waypoints.append(copy.deepcopy(wpose)) #add to plan

#(pen off paper) move to middle of left / line of A
wpose.position.y -= scale * 0.3  #Move (y) back 0.3
wpose.position.z += scale * 0.2 #and (z) up 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#last line dash -
wpose.position.y += scale * 0.2 #Move (y) sideways 0.2
waypoints.append(copy.deepcopy(wpose)) # add to plan

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space
(plan, fraction) = group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

group.execute(plan, wait=True) #execute plan

#Begin planning letter L
#Plan starting pose of L
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.6

#plan and move to target pose
group.set_pose_target(pose_goal)
plan = group.go(wait=True)
group.stop()
group.clear_pose_targets()

waypoints = []
scale = 1 # 1 cm

#Left line down |
wpose = group.get_current_pose().pose #plan from starting pose
wpose.position.z -= scale * 0.5  # Move down (z) by 0.5
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Bottom line right _
wpose.position.y += scale * 0.2  # Move right (y) by 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#compute cartesian path
(plan, fraction) = group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

group.execute(plan, wait=True) #execute plan

#Begin planning letter D
#Plan starting pose of D
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.1

#plan and move to target pose
group.set_pose_target(pose_goal)
plan = group.go(wait=True)
group.stop()
group.clear_pose_targets()

waypoints = []
scale = 1 # 1 cm

#Left line up |
wpose = group.get_current_pose().pose #plan from starting pose
wpose.position.z += scale * 0.5  # Move up (z) by 0.5
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Top line right -
wpose.position.y += scale * 0.2  # Move right (y) by 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Slant down \
wpose.position.y += scale * 0.1  # Move right (y) by 0.1
wpose.position.z -= scale * 0.1  # and down (z) by 0.1
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Right line down |
wpose.position.z -= scale * 0.2  # Move down (z) by 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Slant down /
wpose.position.y -= scale * 0.1  # Move left (y) by 0.1
wpose.position.z -= scale * 0.1  # and down (z) by 0.1
waypoints.append(copy.deepcopy(wpose)) #add to plan

#Bottom line left -
wpose.position.y -= scale * 0.2  # Move right (y) by 0.2
waypoints.append(copy.deepcopy(wpose)) #add to plan

#compute cartesian path
(plan, fraction) = group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

group.execute(plan, wait=True) #execute plan

moveit_commander.roscpp_shutdown() #reset and end package
